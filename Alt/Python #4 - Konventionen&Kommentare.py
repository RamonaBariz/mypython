# Benennung

# CamelCase: Anfangsbuchstaben groß, alles zusammen
# Konvention z.B. in Java
myLevel = 9000

#Alles klein, mit Unterstrich getrennt
# Für Python offiziell empfohlen
my_level = "over 9000"


# Strings

# Mit "
text = "Hallo!!"

# Mit '

# String enthält "
text3 = 'Er sagte "Halllo!!"'

print(text)
print(text2)
print(text3)
  
  
  
  
  """
  Das hier ist ein Kommentar,
  der über mehrere Zeilen geht,
  ohne dass jedes mal ein #
  geschrieben werden muss.
  """
  
